// Copyright 2017 Lunny Xiao. All rights reserved.
// Use of this source code is governed by a MIT-style
// license that can be found in the LICENSE file.

package main

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net/http"
	"strings"
)

type (
	// Repo information
	Repo struct {
		Owner string
		Name  string
	}

	// Build information
	Build struct {
		Tag      string
		Event    string
		Number   int
		Commit   string
		RefSpec  string
		Branch   string
		Author   string
		Avatar   string
		Message  string
		Email    string
		Status   string
		Link     string
		Started  float64
		Finished float64
	}

	// Config for the plugin.
	Config struct {
		AccessToken string
		Message     string
		Drone       bool
		Lang        string
	}

	// Plugin values.
	Plugin struct {
		Repo   Repo
		Build  Build
		Config Config
	}
)

var (
	feishuURL = "https://open.feishu.cn/open-apis/bot/hook/"
)

type FeishuPayload struct {
	Title string `json:"title"`
	Text  string `json:"text"`
}

func sendFeishuMessage(token, message string) error {
	var payload = FeishuPayload{
		Title: message,
		Text:  message,
	}
	bs, err := json.Marshal(&payload)
	if err != nil {
		return err
	}
	_, err = http.Post(feishuURL+token, "", bytes.NewReader(bs))
	if err != nil {
		return err
	}
	return nil
}

// Exec executes the plugin.
func (p *Plugin) Exec() error {
	if len(p.Config.AccessToken) == 0 {
		log.Println("missing feishu config")
		return errors.New("missing feishu config")
	}

	if p.Config.Drone {
		return sendFeishuMessage(p.Config.AccessToken, p.DroneTemplate())
	}

	if len(p.Config.Message) == 0 {
		log.Println("missing message to send")
		return errors.New("missing message to send")
	}

	return sendFeishuMessage(p.Config.AccessToken, p.Config.Message)
}

func (p *Plugin) getTemplate(event string) string {
	if p.Config.Lang == "zh_CN" {
		switch p.Build.Event {
		case "push":
			return `# [%s](%s)
		
%s 推送到 %s 的 %s 分支 %s`
		case "pull_request":
			return "%s 更新了 %s 合并请求 %s"
		case "tag":
			return "%s 推送了 %s 标签 %s"
		}
	} else {
		switch p.Build.Event {
		case "push":
			return `# [%s](%s)

%s pushed to %s branch %s %s`
		case "pull_request":
			return "%s updated %s pull request %s"
		case "tag":
			return "%s pushed %s tag %s"
		}
	}
	return ""
}

// DroneTemplate is plugin default template for Drone CI.
func (p *Plugin) DroneTemplate() string {
	description := ""

	switch p.Build.Event {
	case "push":
		description = fmt.Sprintf(p.getTemplate(p.Build.Event),
			strings.TrimSpace(p.Build.Message),
			p.Build.Link,
			p.Build.Author,
			p.Repo.Owner+"/"+p.Repo.Name,
			p.Build.Branch,
			p.Build.Status)
	case "pull_request":
		branch := ""
		if p.Build.RefSpec != "" {
			branch = p.Build.RefSpec
		} else {
			branch = p.Build.Branch
		}
		description = fmt.Sprintf(p.getTemplate(p.Build.Event), p.Build.Author, p.Repo.Owner+"/"+p.Repo.Name, branch)
	case "tag":
		description = fmt.Sprintf(p.getTemplate(p.Build.Event), p.Build.Author, p.Repo.Owner+"/"+p.Repo.Name, p.Build.Branch)
	}

	return description
}
