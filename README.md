# drone-[feishu](https://feishu.cn)

Drone plugin for sending message to Feishu using Webhook.

[![Build Status](https://drone.gitea.com/api/badges/lunny/tango/status.svg)](https://drone.gitea.com/lunny/drone-feishu) [![](http://gocover.io/_badge/gitea.com/lunny/drone-feishu)](http://gocover.io/gitea.com/lunny/drone-feishu)
[![](https://goreportcard.com/badge/gitea.com/lunny/drone-feishu)](https://goreportcard.com/report/gitea.com/lunny/drone-feishu)

Webhooks are a low-effort way to post messages to group in Feishu. They do not require a bot user or authentication to use. See more [API document information](https://getfeishu.cn/hc/zh-cn/articles/360024984973-%E5%9C%A8%E7%BE%A4%E8%81%8A%E4%B8%AD%E4%BD%BF%E7%94%A8%E6%9C%BA%E5%99%A8%E4%BA%BA).

Sending feishu message using a binary, docker or [Drone CI](http://docs.drone.io/).

## Usage

### Usage from binary

```bash
drone-feishu \
feishu-drone \
  --access_token=xxxx \
  --message="test message"
```

### Usage from docker

```bash
docker run --rm \
  -e FEISHU_ACCESS_TOKEN=xxxx \
  -e MESSAGE=test \
  lunny/drone-feishu
```