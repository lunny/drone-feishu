module gitea.com/lunny/feishu-drone

go 1.13

require (
	github.com/joho/godotenv v1.3.0
	github.com/urfave/cli v1.22.3
)
