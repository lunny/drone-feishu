###################################
#Build stage
FROM golang:1.13-alpine3.10 AS build-env

ARG VERSION

#Build deps
RUN sed -i 's/dl-cdn.alpinelinux.org/mirrors.aliyun.com/g' /etc/apk/repositories
RUN apk --no-cache add build-base git

#Setup repo
COPY ./ /drone-feishu
WORKDIR /drone-feishu

RUN go build -mod=vendor

FROM plugins/base:linux-amd64
LABEL maintainer="xiaolunwen@gmail.com"

ENTRYPOINT ["/app/drone-feishu/drone-feishu"]

COPY --from=build-env /drone-feishu/drone-feishu /app/drone-feishu/drone-feishu